@extends('Layouts.SuperAdminDashboard')

@section('content')
<div class="col-md-12">
    <div class="row">
    	<div class="col-sm-6">
	      	<div class="ibox float-e-margins">
	      		<div class="ibox-title">
	      			<h2>Add Notification</h2>
	      		</div>
	      		<div class="ibox-content">
	      			@if(isset($notification))
	  				<form action="{{url('/notification-update')}}" method="post">
  					@else
  					<form action="{{url('/notification-submit')}}" method="post">
  					@endif
	  					@csrf
	  					@if(isset($notification))
	  					<input type="hidden" name="notification_id" value="{{$notification->notification_id}}">
	  					@endif
	  					@include('message')
	  					<div class="form-group">
	  						<label>Title</label>
	  						<input type="text" name="title" class="form-control" value="@if(isset($notification)){{$notification->title}}@endif">
	  					</div>
	  					<div class="form-group">
	  						<label>Message</label>
	  						<textarea class="form-control" name="message">@if(isset($notification)){{$notification->message}}@endif</textarea>
	  					</div>
	  					<div class="form-group">
	  						<label>Link</label>
	  						<input type="text" name="link" class="form-control" value="@if(isset($notification)){{$notification->link}}@endif">
			  			</div>
	  					<div class="form-group">
	  						<div class="row">
	  							<div class="col-sm-5">
	  								<button class="btn btn-success" type="submit">Submit</button>
	  							</div>
	  						</div>
	  					</div>
	  				</form>
	      		</div>
	      	</div>
	    </div>
        <div class="col-sm-6">
        	<div class="ibox">
        		<div class="ibox-content">
        			<table class="table table-bordered table-responsive text-center" id="notification_table">
	            		<thead>
	            			<tr>
	            				<th class="text-center">Title</th>
	            				<th class="text-center">Message</th>
	            				<th class="text-center">Send At</th>
	            				<th class="text-center">Status</th>
	            				<th class="text-center">Action</th>
	            			</tr>
	            		</thead>
        			</table>
        		</div>
        	</div>		
        </div>
    </div>
</div>                 
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script type="text/javascript">
	$(document).ready( function () {

 	$('#notification_table').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: "{{ url('/datatable/notification_datatable') }}",
          type: 'GET',
          data: function (d) {
          }
         },
         columns: [
                  { data: 'title',name:'title'},
                  { data: 'message',orderable: false,searchable:false},
                  { data: 'send_at',orderable: false,searchable:false},
                  { data: 'status',orderable: false,searchable:false},
                  { data: 'action',orderable: false,searchable:false}
               ]
      });
   });
	$(document).on("click",".notification_delete", function(){
		var notification_id = $(this).attr('notification_id');
		swal({
			title: "Delete",
			text: "Are you sure?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {
				
				var url = '{{ route("notification-delete", ":id") }}';
				url = url.replace(':id', notification_id);
				window.location.href=url;
			}
		});
	});
</script>
@endsection