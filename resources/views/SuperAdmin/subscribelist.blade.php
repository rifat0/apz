@extends('Layouts.SuperAdminDashboard')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
        	<div class="ibox float-e-margins">
				<div class="ibox-title">
        			<h2>Subscribe List</h2>
				</div>
                <div class="ibox-content">
                	<table class="table table-bordered table-striped dataTable" id="subscription_log">
						<thead>
							<tr>
								<th class="text-center">Subscribe ID</th>
								<th class="text-center">User</th>
								<th class="text-center">Agent</th>
								<th class="text-center">Software</th>
								<th class="text-center">Software Variation</th>
								<th class="text-center">Subscribe Date</th>
								<th class="text-center">Activate</th>
								<th class="text-center">Amount</th>
								<th class="text-center">Status</th>
								<th class="text-center">Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script>

	$(document).ready( function () {
		$('#subscription_log').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: {
	        	url: '{{ url("/datatable/subscription_log/")}}',
	        	type: 'GET'
	        },

			dom: '<"html5buttons"B>lTfgitp',
			buttons: [
			{extend: 'copy'},
			{extend: 'csv'},
			{extend: 'excel', title: 'Excel'},
			{extend: 'pdf', title: 'Pdf'},
			{extend: 'print',
				customize: function (win){
					$(win.document.body).addClass('white-bg');
					$(win.document.body).css('font-size', '10px');
					
					$(win.document.body).find('table')
					.addClass('compact')
					.css('font-size', 'inherit');
				}
			}
			],
	        columns: [
                 { data: 'subscribe_id' },
                 { data: 'user',orderable: false,searchable: false},
                 { data: 'agent',orderable: false,searchable: false},
                 { data: 'software',name:'softwareDetails.software_title',orderable: false},
                 { data: 'variation_name',name:'softwareVariationDetails.software_variation_name',orderable: false},
                 { data: 'subscribe_date',orderable: false},
                 { data: 'subscribe_activation_date',orderable: false},
                 { data: 'subscribe_amount'},
                 { data: 'subscribe_status',name:'subscribe_status',searchable: false},
                 { data: 'action',orderable: false,searchable: false}
              ]
	     });
	});

</script>
@endsection